<?php

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Boot
{

    public Static function RunApplication()
    {
        $controller = "Home";
        $method = "index";

        $templateLoader = new FilesystemLoader(getcwd() . '/resources/template_files');
        $tplEngine = new Environment($templateLoader);


        if (!empty($_GET)) {
            $urlPath = trim(array_keys($_GET)[0], "/");
            $urlParser = explode("/", $urlPath);

            if (count($urlParser) > 1) {
                $controller = ucfirst($urlParser[0]);
                $method = $urlParser[1];
            } else {
                $controller = ucfirst($urlParser[0]);
            }


        }

        if (file_exists(getcwd() . "/core/Controllers/{$controller}Controller.php")) {
            $controllerName = "\App\Controllers\\" . $controller . "Controller";
            $controller = new $controllerName();
            if (method_exists($controller, $method)) {
                $data = $controller->$method();
                $template = $tplEngine->load($data[0] . '.tpl');

                echo $template->render(isset($data[1]) ? $data[1] : []);
                exit();
            }
        }


        $template = $tplEngine->load('404.tpl');
        echo $template->render();
        exit;
    }

}
