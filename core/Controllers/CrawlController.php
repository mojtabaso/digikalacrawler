<?php


namespace App\Controllers;


use App\classes\modules\Digikala;


class CrawlController
{

    public function index()
    {
        if (isset($_POST) && !is_null($_POST['dkproduct'])) {
            if (strpos($_POST['dkproduct'], "digikala.com") !== false) {
                $digiKalaProductUrl = $_POST['dkproduct'];
                if ($this->HelperproductChecker($digiKalaProductUrl)) {
                    $digiKalaObject = new Digikala("$digiKalaProductUrl");
                    $data = $digiKalaObject->getAnalysData();
                    return array("details", $data);
                } else {
                    return array("404.digikala");
                }
            }
        }

        //return array('404');
    }

    public function HelperproductChecker($productUrl)
    {
        $handle = curl_init($productUrl);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        if ($httpCode == 404) {
            return false;
        }
        curl_close($handle);
        return true;
    }

}