<?php

namespace App\classes\modules;

use PHPHtmlParser\Dom;

class Digikala
{
    private $domDocument;
    private $htmlSource;

    public function __construct($dkUrl)
    {
        $this->domDocument = new Dom;
        $this->domDocument->loadFromUrl($dkUrl);
        $this->htmlSource = $this->domDocument->load($this->domDocument->outerHtml);
    }

    public function getAnalysData()
    {
        return array(
            "title" => $this->getProductTitle(),
            "image" => $this->getProductImage(),
            "price" => $this->getProductPrice(),
            "seller" => $this->getProductSeller(),
        );
    }

    private function getProductTitle()
    {
        return ($this->htmlSource->find('.c-product__title'))->text;
    }

    private function getProductImage()
    {
        $image = $this->htmlSource->find('.c-gallery__img')[0];
        $image = $image->firstChild();
        return $image->getAttribute('data-src');
    }

    private function getProductPrice()
    {
        return ($this->htmlSource->find(".js-price-value"))->text;
    }

    private function getProductSeller()
    {
        return ($this->htmlSource->find(".js-seller-url"))->text;
    }


    public function __destruct()
    {
        unset($this->domDocument);
        unset($this->htmlSource);
    }

}