<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* details.tpl */
class __TwigTemplate_aca74878efc978eb47206f5a5b695aff9d1ac33efe7e218b374bfc805c5ae7aa extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!doctype html>
<html lang=\"en\" dir=\"rtl\">

<head>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <title>روبوکالا آنالیزور هوشمند دیجیکالا</title>


    <link
            rel=\"stylesheet\"
            href=\"https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css\"
            integrity=\"sha384-vus3nQHTD+5mpDiZ4rkEPlnkcyTP+49BhJ4wJeJunw06ZAp+wzzeBPUXr42fi8If\"
            crossorigin=\"anonymous\">

    <!-- Styles -->
    <style>
        @font-face {
            font-family: IRANSans;
            font-style: normal;
            font-weight: bold;
            src: url('http://resellha.ir/fonts/iransans/eot/IRANSansWeb_Bold.eot');
            src: url('http://resellha.ir/fonts/iransans/eot/IRANSansWeb_Bold.eot?#iefix') format('embedded-opentype'), /* IE6-8 */ url('http://resellha.ir/fonts/iransans/woff2/IRANSansWeb_Bold.woff2') format('woff2'), /* FF39+,Chrome36+, Opera24+*/ url('http://resellha.ir/fonts/iransans/woff/IRANSansWeb_Bold.woff') format('woff'), /* FF3.6+, IE9, Chrome6+, Saf5.1+*/ url('http://resellha.ir/fonts/iransans/ttf/IRANSansWeb_Bold.ttf') format('truetype');
        }

        @font-face {
            font-family: IRANSans;
            font-style: normal;
            font-weight: 500;
            src: url('http://resellha.ir/fonts/iransans/eot/IRANSansWeb_Medium.eot');
            src: url('http://resellha.ir/fonts/iransans/eot/IRANSansWeb_Medium.eot?#iefix') format('embedded-opentype'), /* IE6-8 */ url('http://resellha.ir/fonts/iransans/woff2/IRANSansWeb_Medium.woff2') format('woff2'), /* FF39+,Chrome36+, Opera24+*/ url('http://resellha.ir/fonts/iransans/woff/IRANSansWeb_Medium.woff') format('woff'), /* FF3.6+, IE9, Chrome6+, Saf5.1+*/ url('http://resellha.ir/fonts/iransans/ttf/IRANSansWeb_Medium.ttf') format('truetype');
        }

        @font-face {
            font-family: IRANSans;
            font-style: normal;
            font-weight: 300;
            src: url('http://resellha.ir/fonts/iransans/eot/IRANSansWeb_Light.eot');
            src: url('http://resellha.ir/fonts/iransans/eot/IRANSansWeb_Light.eot?#iefix') format('embedded-opentype'), /* IE6-8 */ url('http://resellha.ir/fonts/iransans/woff2/IRANSansWeb_Light.woff2') format('woff2'), /* FF39+,Chrome36+, Opera24+*/ url('http://resellha.ir/fonts/iransans/woff/IRANSansWeb_Light.woff') format('woff'), /* FF3.6+, IE9, Chrome6+, Saf5.1+*/ url('http://resellha.ir/fonts/iransans/ttf/IRANSansWeb_Light.ttf') format('truetype');
        }

        @font-face {
            font-family: IRANSans;
            font-style: normal;
            font-weight: 200;
            src: url('http://resellha.ir/fonts/iransans/eot/IRANSansWeb_UltraLight.eot');
            src: url('http://resellha.ir/fonts/iransans/eot/IRANSansWeb_UltraLight.eot?#iefix') format('embedded-opentype'), /* IE6-8 */ url('http://resellha.ir/fonts/iransans/woff2/IRANSansWeb_UltraLight.woff2') format('woff2'), /* FF39+,Chrome36+, Opera24+*/ url('http://resellha.ir/fonts/iransans/woff/IRANSansWeb_UltraLight.woff') format('woff'), /* FF3.6+, IE9, Chrome6+, Saf5.1+*/ url('http://resellha.ir/fonts/iransans/ttf/IRANSansWeb_UltraLight.ttf') format('truetype');
        }

        @font-face {
            font-family: IRANSans;
            font-style: normal;
            font-weight: normal;
            src: url('http://resellha.ir/fonts/iransans/eot/IRANSansWeb.eot');
            src: url('http://resellha.ir/fonts/iransans/eot/IRANSansWeb.eot?#iefix') format('embedded-opentype'), /* IE6-8 */ url('http://resellha.ir/fonts/iransans/woff2/IRANSansWeb.woff2') format('woff2'), /* FF39+,Chrome36+, Opera24+*/ url('http://resellha.ir/fonts/iransans/woff/IRANSansWeb.woff') format('woff'), /* FF3.6+, IE9, Chrome6+, Saf5.1+*/ url('http://resellha.ir/fonts/iransans/ttf/IRANSansWeb.ttf') format('truetype');
        }

        body {
            font-family: 'iransans', 'Tahoma' !important;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .center {
            margin: auto;
            text-align: center;
            display: inline-block;

        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .container footer {
            font-size: 9pt;
            color: #555;
            margin-bottom: 30px;

        }

        .container footer ul.links {
            margin: 0;
            padding: 0;
        }

        .container footer ul.links li {
            padding: 0;
            margin: 0 0 0 30px;
            list-style-type: none;
            float: right;
            font-family: \"iransans\", \"Tahoma\";
        }


    </style>
</head>

<body>

<nav class=\"navbar navbar-expand-md navbar-dark bg-primary mb-4\">
    <a class=\"navbar-brand\" href=\"#\">روبو کالا </a>
    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarsExample03\"
            aria-controls=\"navbarsExample03\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
    </button>

    <div class=\"collapse navbar-collapse\" id=\"navbarsExample03\">
        <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-item active\">
                <a class=\"nav-link\" href=\"#\">صفحه اصلی <span class=\"sr-only\">(current)</span></a>
            </li>


            <li class=\"nav-item active\">
                <a class=\"nav-link\" href=\"#\">لیست قیمت</a>
            </li>

            <li class=\"nav-item active\">
                <a class=\"nav-link\" href=\"#\">قوانین</a>
            </li>
            <li class=\"nav-item active\">
                <a class=\"nav-link\" href=\"#\">تماس با ما</a>
            </li>

            <li class=\"nav-item dropdown active\">
                <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"dropdown03\" data-toggle=\"dropdown\" aria-haspopup=\"true\"
                   aria-expanded=\"false\">حساب کاربری</a>
                <ul class=\"dropdown-menu\">
                    <li class=\"dropdown-header\">کنترل</li>
                    <li class=\"dropdown-item\"><a href=\"#\">ورود و خروج کاربر</a></li>
                </ul>
            </li>

        </ul>
    </div>
</nav>

<main class=\"mt-5 pt-4\">
    <div class=\"container dark-grey-text mt-5\">

        <!--Grid row-->
        <div class=\"row wow fadeIn\" style=\"visibility: visible; animation-name: fadeIn;\">

            <!--Grid column-->
            <div class=\"col-md-6 mb-4\">

                <img src=\"";
        // line 185
        echo twig_escape_filter($this->env, ($context["image"] ?? null), "html", null, true);
        echo "\" class=\"img-fluid\" alt=\"\">

            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class=\"col-md-6 mb-4\">

                <!--Content-->
                <div class=\"p-4\">

                    <div class=\"mb-3\">
                        <a href=\"\">
                            <span class=\"badge purple mr-1\">Category 2</span>
                        </a>
                        <a href=\"\">
                            <span class=\"badge blue mr-1\">New</span>
                        </a>
                        <a href=\"\">
                            <span class=\"badge red mr-1\">Bestseller</span>
                        </a>
                    </div>

                    <p class=\"lead\">
              <span class=\"mr-1\">
                <del>\$200</del>
              </span>
                        <span>\$100</span>
                    </p>

                    <p class=\"lead font-weight-bold\">Description</p>

                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et dolor suscipit libero eos atque quia
                        ipsa
                        sint voluptatibus!
                        Beatae sit assumenda asperiores iure at maxime atque repellendus maiores quia sapiente.</p>

                    <form class=\"d-flex justify-content-left\">
                        <!-- Default input -->
                        <input type=\"number\" value=\"1\" aria-label=\"Search\" class=\"form-control\" style=\"width: 100px\">
                        <button class=\"btn btn-primary btn-md my-0 p waves-effect waves-light\" type=\"submit\">Add to cart
                            <i class=\"fas fa-shopping-cart ml-1\"></i>
                        </button>

                    </form>

                </div>
                <!--Content-->

            </div>
            <!--Grid column-->

        </div>
        <!--Grid row-->

        <hr>

        <!--Grid row-->
        <div class=\"row d-flex justify-content-center wow fadeIn\" style=\"visibility: visible; animation-name: fadeIn;\">

            <!--Grid column-->
            <div class=\"col-md-6 text-center\">

                <h4 class=\"my-4 h4\">Additional information</h4>

                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus suscipit modi sapiente illo soluta
                    odit
                    voluptates,
                    quibusdam officia. Neque quibusdam quas a quis porro? Molestias illo neque eum in laborum.</p>

            </div>
            <!--Grid column-->

        </div>
        <!--Grid row-->

        <!--Grid row-->
        <div class=\"row wow fadeIn\" style=\"visibility: visible; animation-name: fadeIn;\">

            <!--Grid column-->
            <div class=\"col-lg-4 col-md-12 mb-4\">

                <img src=\"https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/11.jpg\" class=\"img-fluid\"
                     alt=\"\">

            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class=\"col-lg-4 col-md-6 mb-4\">

                <img src=\"https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/12.jpg\" class=\"img-fluid\"
                     alt=\"\">

            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class=\"col-lg-4 col-md-6 mb-4\">

                <img src=\"https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/13.jpg\" class=\"img-fluid\"
                     alt=\"\">

            </div>
            <!--Grid column-->

        </div>
        <!--Grid row-->

    </div>
</main>


<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>

<script
        src=\"https://cdn.rtlcss.com/bootstrap/v4.2.1/js/bootstrap.min.js\"
        integrity=\"sha384-a9xOd0rz8w0J8zqj1qJic7GPFfyMfoiuDjC9rqXlVOcGO/dmRqzMn34gZYDTel8k\"
        crossorigin=\"anonymous\"></script>

</body>
</html>";
    }

    public function getTemplateName()
    {
        return "details.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  223 => 185,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "details.tpl", "/var/www/html/dk/public_html/resources/template_files/details.tpl");
    }
}
