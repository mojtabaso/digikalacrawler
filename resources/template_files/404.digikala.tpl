{% include 'parts/header.tpl' %}

<main role="main" class="mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">کالای ناموجود</div>
                    <div class="card-body">
                        <h1 class="card-text text-center">
                            کالای درخواستی شما در دیجیکالا یافت نشد
                        </h1>
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>



{% include 'parts/footer.tpl' %}
