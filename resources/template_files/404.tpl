{% include 'parts/header.tpl' %}

<main role="main" class="mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        404 Page not found
                    </div>
                    <div class="card-body">
                        <h1 class="card-text text-center">
                           صفحه درخواستی شما یافت نشد.
                        </h1>
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>



{% include 'parts/footer.tpl' %}
