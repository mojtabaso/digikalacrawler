{% include 'parts/header.tpl' %}


<main class="mb-5">

    <div class="container dark-grey-text md-12">

        <div class="card">
            <div class="card-header">
                {{title}}
            </div>
            <div class="card-body">
                <!--Grid row-->
                <div class="row wow fadeIn" style="visibility: visible; animation-name: fadeIn;">

                    <!--Grid column-->
                    <div class="col-md-6 mb-4">

                        <img src="{{ image }}" class="img-fluid" alt="">

                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-6 mb-4">

                        <!--Content-->
                        <div class="p-4">

                            <div class="mb-3">


                                <a href="">
                                    <span class="badge red mr-1">                {{title}}
</span>
                                </a>
                            </div>

                            <p class="lead">
              <span class="mr-1">
                  قیمت :
                {{price}} تومان
              </span>
                            </p>

                            <p class="lead font-weight-bold">فروشنده : {{seller}}
                            </p>


                        </div>
                        <!--Content-->

                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->
            </div>

        </div>


    </div>
</main>

{% include 'parts/footer.tpl' %}
