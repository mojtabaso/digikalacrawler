{% include 'parts/header.tpl' %}

<main role="main" class="mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="flex-center position-ref full-height">
                    <div class="container-fluid">
                        <div class="row text-center">
                            <div class="col-md-12 text-center">
                                <form class="form-inline" action="crawl" method="POST">
                                    <div class="input-group col-md-10 m-auto">
                                        <input type="text" class="form-control" name="dkproduct"
                                               placeholder="برای مثال: https://www.digikala.com/product/dkp-456520"
                                               autocapitalize="none" data-toggle="tooltip" data-placement="left"
                                               data-trigger="manual"
                                               title="" data-original-title="ضروری">
                                        <input type="submit" name="transfer" class="btn btn-primary margin-l-10"
                                               value="بررسی">

                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

{% include 'parts/footer.tpl' %}
