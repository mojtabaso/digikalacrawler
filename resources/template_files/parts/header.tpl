<!doctype html>
<html lang="en" dir="rtl">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>روبوکالا</title>



    <link
            rel="stylesheet"
            href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css"
            integrity="sha384-vus3nQHTD+5mpDiZ4rkEPlnkcyTP+49BhJ4wJeJunw06ZAp+wzzeBPUXr42fi8If"
            crossorigin="anonymous">

    <!-- Styles -->
    <style>
        @font-face {
            font-family: IRANSans;
            font-style: normal;
            font-weight: bold;
            src: url('resources/fonts/iransans/eot/IRANSansWeb_Bold.eot');
            src: url('resources/fonts/iransans/eot/IRANSansWeb_Bold.eot?#iefix') format('embedded-opentype'), /* IE6-8 */ url('resources/fonts/iransans/woff2/IRANSansWeb_Bold.woff2') format('woff2'), /* FF39+,Chrome36+, Opera24+*/ url('resources/fonts/iransans/woff/IRANSansWeb_Bold.woff') format('woff'), /* FF3.6+, IE9, Chrome6+, Saf5.1+*/ url('resources/fonts/iransans/ttf/IRANSansWeb_Bold.ttf') format('truetype');
        }

        @font-face {
            font-family: IRANSans;
            font-style: normal;
            font-weight: 500;
            src: url('resources/fonts/iransans/eot/IRANSansWeb_Medium.eot');
            src: url('resources/fonts/iransans/eot/IRANSansWeb_Medium.eot?#iefix') format('embedded-opentype'), /* IE6-8 */ url('resources/fonts/iransans/woff2/IRANSansWeb_Medium.woff2') format('woff2'), /* FF39+,Chrome36+, Opera24+*/ url('resources/fonts/iransans/woff/IRANSansWeb_Medium.woff') format('woff'), /* FF3.6+, IE9, Chrome6+, Saf5.1+*/ url('resources/fonts/iransans/ttf/IRANSansWeb_Medium.ttf') format('truetype');
        }

        @font-face {
            font-family: IRANSans;
            font-style: normal;
            font-weight: 300;
            src: url('resources/fonts/iransans/eot/IRANSansWeb_Light.eot');
            src: url('resources/fonts/iransans/eot/IRANSansWeb_Light.eot?#iefix') format('embedded-opentype'), /* IE6-8 */ url('resources/fonts/iransans/woff2/IRANSansWeb_Light.woff2') format('woff2'), /* FF39+,Chrome36+, Opera24+*/ url('resources/fonts/iransans/woff/IRANSansWeb_Light.woff') format('woff'), /* FF3.6+, IE9, Chrome6+, Saf5.1+*/ url('resources/fonts/iransans/ttf/IRANSansWeb_Light.ttf') format('truetype');
        }

        @font-face {
            font-family: IRANSans;
            font-style: normal;
            font-weight: 200;
            src: url('resources/fonts/iransans/eot/IRANSansWeb_UltraLight.eot');
            src: url('resources/fonts/iransans/eot/IRANSansWeb_UltraLight.eot?#iefix') format('embedded-opentype'), /* IE6-8 */ url('resources/fonts/iransans/woff2/IRANSansWeb_UltraLight.woff2') format('woff2'), /* FF39+,Chrome36+, Opera24+*/ url('resources/fonts/iransans/woff/IRANSansWeb_UltraLight.woff') format('woff'), /* FF3.6+, IE9, Chrome6+, Saf5.1+*/ url('resources/fonts/iransans/ttf/IRANSansWeb_UltraLight.ttf') format('truetype');
        }

        @font-face {
            font-family: IRANSans;
            font-style: normal;
            font-weight: normal;
            src: url('resources/fonts/iransans/eot/IRANSansWeb.eot');
            src: url('resources/fonts/iransans/eot/IRANSansWeb.eot?#iefix') format('embedded-opentype'), /* IE6-8 */ url('resources/fonts/iransans/woff2/IRANSansWeb.woff2') format('woff2'), /* FF39+,Chrome36+, Opera24+*/ url('resources/fonts/iransans/woff/IRANSansWeb.woff') format('woff'), /* FF3.6+, IE9, Chrome6+, Saf5.1+*/ url('resources/fonts/iransans/ttf/IRANSansWeb.ttf') format('truetype');
        }

        body {
            font-family: 'iransans', 'Tahoma' !important;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .center {
            margin: auto;
            text-align: center;
            display: inline-block;

        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .container footer {
            font-size: 9pt;
            color: #555;
            margin-bottom: 30px;

        }

        .container footer ul.links {
            margin: 0;
            padding: 0;
        }

        .container footer ul.links li {
            padding: 0;
            margin: 0 0 0 30px;
            list-style-type: none;
            float: right;
            font-family: "iransans", "Tahoma";
        }


    </style>
</head>

<body>

<nav class="navbar navbar-expand-md navbar-dark bg-primary mb-4">
    <a class="navbar-brand" href="#">روبو کالا </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03"
            aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExample03">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">صفحه اصلی <span class="sr-only">(current)</span></a>
            </li>




            <li class="nav-item active">
                <a class="nav-link" href="#">لیست قیمت</a>
            </li>

            <li class="nav-item active">
                <a class="nav-link" href="#">قوانین</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="#">تماس با ما</a>
            </li>

            <li class="nav-item dropdown active">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown03" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">حساب کاربری</a>
                <ul class="dropdown-menu">
                    <li class="dropdown-header">کنترل</li>
                    <li class="dropdown-item"><a href="#">ورود و خروج کاربر</a></li>
                </ul>
            </li>

        </ul>
    </div>
</nav>
